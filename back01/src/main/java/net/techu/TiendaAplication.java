package net.techu;


//import com.example.formacion1.DemoApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TiendaAplication {
    public static void main(String[] args) {
        SpringApplication.run(TiendaAplication.class, args);
    }
}
